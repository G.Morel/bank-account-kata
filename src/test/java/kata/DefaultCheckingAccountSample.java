package kata;

import kata.formatters.AnteChronologicalFormatter;
import kata.operations.Operation;
import kata.operations.OperationType;
import kata.printers.Printer;
import kata.printers.StreamPrinter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DefaultCheckingAccountSample {

    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private DefaultCheckingAccount checkingAccount;

    private OutputStream outputStream = new ByteArrayOutputStream();

    private Printer printer = new StreamPrinter(new PrintStream(outputStream), new AnteChronologicalFormatter());

    @Test
    @DisplayName("Simple scenario")
    void sample() {
        final var accountId = "12345";

        when(accountRepository.accountExists(accountId)).thenReturn(true);

        // 24/05/2017
        Assertions.assertDoesNotThrow(() -> checkingAccount.deposit(accountId, BigDecimal.valueOf(1000, 2)));

        // 27/05/2017
        Assertions.assertDoesNotThrow(() -> checkingAccount.deposit(accountId, BigDecimal.valueOf(5599, 2)));

        when(accountRepository.getBalance(accountId)).thenReturn(BigDecimal.valueOf(6599, 2));

        // 01/06/2017
        Assertions.assertDoesNotThrow(() -> checkingAccount.withdraw(accountId, BigDecimal.valueOf(2500, 2)));

        when(accountRepository.accountExists(accountId)).thenReturn(true);
        when(accountRepository.findOperations(accountId)).thenReturn(
                List.of(
                        new Operation(BigDecimal.valueOf(1000, 2), BigDecimal.valueOf(1000, 2), Instant.ofEpochSecond(1495620000), OperationType.DEPOSIT),
                        new Operation(BigDecimal.valueOf(5599, 2), BigDecimal.valueOf(6599, 2), Instant.ofEpochSecond(1495879200), OperationType.DEPOSIT),
                        new Operation(BigDecimal.valueOf(2500, 2), BigDecimal.valueOf(4099, 2), Instant.ofEpochSecond(1496311200), OperationType.WITHDRAW)
                )
        );

        final var expectedStatement =
                "|         DATE |       CREDIT |        DEBIT |      BALANCE |" + System.lineSeparator() +
                        "|   01/06/2017 |              |        25,00 |        40,99 |" + System.lineSeparator() +
                        "|   27/05/2017 |        55,99 |              |        65,99 |" + System.lineSeparator() +
                        "|   24/05/2017 |        10,00 |              |        10,00 |" + System.lineSeparator();

        Assertions.assertDoesNotThrow(() -> checkingAccount.printBankStatement(accountId, printer));
        Assertions.assertEquals(expectedStatement, outputStream.toString());
    }
}
