package kata.operations;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.stream.Stream;

class OperationTest {

    private static Stream<Arguments> operationArguments() {
        return Stream.of(
                Arguments.of(BigDecimal.valueOf(2477, 2), BigDecimal.valueOf(2000, 2), Instant.ofEpochSecond(2379842), OperationType.DEPOSIT),
                Arguments.of(BigDecimal.valueOf(9, 2), BigDecimal.valueOf(37645, 2), Instant.ofEpochSecond(314234), OperationType.DEPOSIT),
                Arguments.of(BigDecimal.valueOf(382, 2), BigDecimal.valueOf(379, 2), Instant.ofEpochSecond(38478923), OperationType.WITHDRAW),
                Arguments.of(BigDecimal.valueOf(23428, 2), BigDecimal.valueOf(9827, 2), Instant.ofEpochSecond(984636), OperationType.WITHDRAW)
        );
    }

    @ParameterizedTest(name = "{3} of {0} money with new balance {1} at {2}")
    @MethodSource("operationArguments")
    @DisplayName("Should be initialized properly")
    void shouldBeInitializedProperly(BigDecimal amount, BigDecimal balance, Instant date, OperationType operationType) {
        final var operation = new Operation(amount, balance, date, operationType);

        Assertions.assertAll(
                () -> Assertions.assertEquals(amount, operation.getAmount()),
                () -> Assertions.assertEquals(balance, operation.getBalance()),
                () -> Assertions.assertEquals(date, operation.getDate()),
                () -> Assertions.assertEquals(operationType, operation.getType())
        );
    }

    @Test
    @DisplayName("Should support equals and hashcode")
    void shouldSupportEqualsAndHashCode() {
        EqualsVerifier.forClass(Operation.class).verify();
    }
}