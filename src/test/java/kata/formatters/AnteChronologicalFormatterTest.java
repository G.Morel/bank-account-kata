package kata.formatters;

import kata.operations.Operation;
import kata.operations.OperationType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

class AnteChronologicalFormatterTest {

    // 25/07/2019 14:22:06
    private static final Instant firstDate = Instant.now(Clock.fixed(Instant.ofEpochSecond(1564064526), ZoneId.of("UTC")));
    // 26/07/2019 10:31:23
    private static final Instant secondDate = Instant.now(Clock.fixed(Instant.ofEpochSecond(1564137083), ZoneId.of("UTC")));
    // 30/07/2019 10:31:23
    private static final Instant thirdDate = Instant.now(Clock.fixed(Instant.ofEpochSecond(1564482683), ZoneId.of("UTC")));

    private static final String EXPECTED_HEADER = "|         DATE |       CREDIT |        DEBIT |      BALANCE |" + System.lineSeparator();

    private final AnteChronologicalFormatter formatter = new AnteChronologicalFormatter();

    private static Stream<Arguments> deposits() {
        return Stream.of(
                Arguments.of(Collections.singletonList(
                        new Operation(BigDecimal.valueOf(9999, 2), BigDecimal.valueOf(9999, 2), firstDate, OperationType.DEPOSIT)),
                        "|         DATE |       CREDIT |        DEBIT |      BALANCE |" + System.lineSeparator() +
                                "|   25/07/2019 |        99,99 |              |        99,99 |" + System.lineSeparator()),
                Arguments.of(Collections.singletonList(
                        new Operation(BigDecimal.valueOf(99, 2), BigDecimal.valueOf(199, 2), secondDate, OperationType.DEPOSIT)),
                        "|         DATE |       CREDIT |        DEBIT |      BALANCE |" + System.lineSeparator() +
                                "|   26/07/2019 |         0,99 |              |         1,99 |" + System.lineSeparator())
        );
    }

    private static Stream<Arguments> withdrawals() {
        return Stream.of(
                Arguments.of(Collections.singletonList(
                        new Operation(BigDecimal.valueOf(9999, 2), BigDecimal.valueOf(9999, 2), secondDate, OperationType.WITHDRAW)),
                        "|         DATE |       CREDIT |        DEBIT |      BALANCE |" + System.lineSeparator() +
                                "|   26/07/2019 |              |        99,99 |        99,99 |" + System.lineSeparator()),
                Arguments.of(Collections.singletonList(
                        new Operation(BigDecimal.valueOf(99, 2), BigDecimal.valueOf(199, 2), thirdDate, OperationType.WITHDRAW)),
                        "|         DATE |       CREDIT |        DEBIT |      BALANCE |" + System.lineSeparator() +
                                "|   30/07/2019 |              |         0,99 |         1,99 |" + System.lineSeparator())
        );
    }

    private static Stream<Arguments> depositsAndWithdrawals() {
        return Stream.of(
                Arguments.of(
                        List.of(
                                new Operation(BigDecimal.valueOf(9999, 2), BigDecimal.valueOf(9999, 2), firstDate, OperationType.DEPOSIT),
                                new Operation(BigDecimal.valueOf(1000, 2), BigDecimal.valueOf(8999, 2), secondDate, OperationType.WITHDRAW),
                                new Operation(BigDecimal.valueOf(100, 2), BigDecimal.valueOf(9099, 2), thirdDate, OperationType.DEPOSIT)
                        ),
                        "|         DATE |       CREDIT |        DEBIT |      BALANCE |" + System.lineSeparator() +
                                "|   30/07/2019 |         1,00 |              |        90,99 |" + System.lineSeparator() +
                                "|   26/07/2019 |              |        10,00 |        89,99 |" + System.lineSeparator() +
                                "|   25/07/2019 |        99,99 |              |        99,99 |" + System.lineSeparator())
        );
    }

    @Test
    @DisplayName("Should prepend with header")
    void shouldPrependWithHeader() {
        final var result = formatter.formatOperations(List.of());
        Assertions.assertEquals(EXPECTED_HEADER, result);
    }

    @ParameterizedTest
    @EmptySource
    @NullSource
    @DisplayName("Should handle null and empty collections")
    void shouldHandleNullAndEmptyCollections(List<Operation> operations) {
        final var result = formatter.formatOperations(operations);
        Assertions.assertEquals(EXPECTED_HEADER, result);
    }

    @ParameterizedTest
    @MethodSource("deposits")
    @DisplayName("Should format deposit correctly")
    void shouldFormatDepositCorrectly(List<Operation> operations, String expectedOutput) {
        final var result = formatter.formatOperations(operations);
        Assertions.assertEquals(expectedOutput, result);
    }

    @ParameterizedTest
    @MethodSource("withdrawals")
    @DisplayName("Should format withdrawal correctly")
    void shouldFormatWithdrawalCorrectly(List<Operation> operations, String expectedOutput) {
        final var result = formatter.formatOperations(operations);
        Assertions.assertEquals(expectedOutput, result);
    }

    @ParameterizedTest
    @MethodSource("depositsAndWithdrawals")
    @DisplayName("Should format operations ordered by descending date")
    void shouldFormatOperationsOrderedByDateDesc(List<Operation> operations, String expectedFormat) {
        final var result = formatter.formatOperations(operations);
        Assertions.assertEquals(expectedFormat, result);
    }
}