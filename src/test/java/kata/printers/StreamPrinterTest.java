package kata.printers;

import kata.formatters.Formatter;
import kata.operations.Operation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StreamPrinterTest {

    private final OutputStream outputStream = new ByteArrayOutputStream();
    private final PrintStream printStream = new PrintStream(outputStream);

    @Mock
    private Formatter formatterMock;
    private Printer printer;

    @BeforeEach
    void setUp() {
        printer = new StreamPrinter(printStream, formatterMock);
    }

    @ParameterizedTest
    @NullSource
    @EmptySource
    @DisplayName("Should print formatted operations")
    void shouldPrintFormattedOperations(List<Operation> operations) {
        final var formattedOperation = "| FormattedOperations |";

        when(formatterMock.formatOperations(operations)).thenReturn(formattedOperation);

        printer.print(operations);

        Assertions.assertEquals(formattedOperation, outputStream.toString());
        verify(formatterMock).formatOperations(operations);
        verifyNoMoreInteractions(formatterMock);
    }

}