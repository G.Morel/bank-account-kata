package kata;

import kata.exceptions.AccountNotFoundException;
import kata.exceptions.LessThanOrEqualZeroAmountException;
import kata.exceptions.NotEnoughFundsException;
import kata.operations.Operation;
import kata.printers.Printer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DefaultCheckingAccountTest {

    @Mock
    private AccountRepository accountRepositoryMock;

    @Mock
    private Printer printerMock;

    private CheckingAccount checkingAccount;

    private static Stream<Arguments> authorizedDeposits() {
        return Stream.of(
                Arguments.of("326", BigDecimal.valueOf(10)),
                Arguments.of("38-76", BigDecimal.valueOf(10))
        );
    }

    private static Stream<Arguments> notAuthorizedWithdrawals() {
        return Stream.of(
                Arguments.of("37293", BigDecimal.valueOf(1000, 2), BigDecimal.valueOf(900, 2)),
                Arguments.of("37293", BigDecimal.valueOf(2500, 2), BigDecimal.valueOf(0, 2)),
                Arguments.of("37293", BigDecimal.valueOf(1001, 2), BigDecimal.valueOf(1000, 2))
        );
    }

    private static Stream<Arguments> authorizedWithdraw() {
        return Stream.of(
                Arguments.of("206", BigDecimal.valueOf(2000, 2), BigDecimal.valueOf(15000, 2), BigDecimal.valueOf(13000, 2)),
                Arguments.of("007", BigDecimal.valueOf(1, 2), BigDecimal.valueOf(1, 2), BigDecimal.valueOf(0, 2)),
                Arguments.of("930", BigDecimal.valueOf(999, 2), BigDecimal.valueOf(1000, 2), BigDecimal.valueOf(1, 2))
        );
    }

    private static Stream<Arguments> lowerOrEqualThanZeroAmount() {
        return Stream.of(
                Arguments.of("128", BigDecimal.valueOf(0, 2)),
                Arguments.of("372", BigDecimal.valueOf(-2000, 2))
        );
    }

    @BeforeEach
    void setup() {
        checkingAccount = new DefaultCheckingAccount(accountRepositoryMock);
    }

    @ParameterizedTest(name = "of {1} on account {0}")
    @MethodSource("authorizedDeposits")
    @DisplayName("Should save authorized deposit")
    void shouldSaveAuthorizedDeposit(String accountId, BigDecimal givenAmount) throws AccountNotFoundException, LessThanOrEqualZeroAmountException {
        when(accountRepositoryMock.accountExists(accountId)).thenReturn(true);
        Assertions.assertDoesNotThrow(() -> checkingAccount.deposit(accountId, givenAmount));

        verify(accountRepositoryMock).deposit(accountId, givenAmount);
        verifyNoMoreInteractions(accountRepositoryMock);
    }

    @Test
    @DisplayName("Should not save deposit when account does not exist")
    void shouldNotSaveDepositWhenAccountDoesNotExist() {
        final var notExistingAccount = "1234";
        final var depositAmount = BigDecimal.valueOf(1234, 2);
        when(accountRepositoryMock.accountExists(notExistingAccount)).thenReturn(false);

        Assertions.assertThrows(AccountNotFoundException.class, () -> checkingAccount.deposit(notExistingAccount, depositAmount));
        verify(accountRepositoryMock).accountExists(notExistingAccount);
        verifyNoMoreInteractions(accountRepositoryMock);
    }

    @ParameterizedTest(name = "of {1} on account {0} with balance {2}")
    @MethodSource("notAuthorizedWithdrawals")
    @DisplayName("Should not authorize withdraw")
    void shouldNotAuthorizeWithdrawWhenNotEnoughFunds(String accountId, BigDecimal givenAmount, BigDecimal existingBalance) {
        when(accountRepositoryMock.accountExists(accountId)).thenReturn(true);
        when(accountRepositoryMock.getBalance(accountId)).thenReturn(existingBalance);

        Assertions.assertThrows(NotEnoughFundsException.class, () -> checkingAccount.withdraw(accountId, givenAmount));

        verify(accountRepositoryMock).getBalance(accountId);
        verifyNoMoreInteractions(accountRepositoryMock);
    }

    @Test
    @DisplayName("Should not authorize withdraw when account does not exist")
    void shouldNotAuthorizeWithdrawWhenAccountDoesNotExist() {
        final var notExistingAccount = "1234";
        final var withdrawAmount = BigDecimal.valueOf(1234, 2);
        when(accountRepositoryMock.accountExists(notExistingAccount)).thenReturn(false);

        Assertions.assertThrows(AccountNotFoundException.class, () -> checkingAccount.withdraw(notExistingAccount, withdrawAmount));

        verify(accountRepositoryMock).accountExists(notExistingAccount);
        verifyNoMoreInteractions(accountRepositoryMock);
    }

    @ParameterizedTest(name = "of {1} on account {0} with balance {2}")
    @MethodSource("authorizedWithdraw")
    @DisplayName("Should save authorized withdraw")
    void shouldSaveAuthorizedWithdraw(String accountId, BigDecimal givenAmount, BigDecimal existingBalance) throws AccountNotFoundException, NotEnoughFundsException, LessThanOrEqualZeroAmountException {
        when(accountRepositoryMock.accountExists(accountId)).thenReturn(true);
        when(accountRepositoryMock.getBalance(accountId)).thenReturn(existingBalance);

        checkingAccount.withdraw(accountId, givenAmount);

        verify(accountRepositoryMock).getBalance(accountId);
        verify(accountRepositoryMock).withdraw(accountId, givenAmount);
        verifyNoMoreInteractions(accountRepositoryMock);
    }

    @ParameterizedTest(name = "of {1} on account {0}")
    @MethodSource("lowerOrEqualThanZeroAmount")
    @DisplayName("Should not authorize withdraw")
    void shouldNotAuthorizeLessThanOrEqualZeroWithdraw(String accountId, BigDecimal givenAmount) {
        when(accountRepositoryMock.accountExists(accountId)).thenReturn(true);

        Assertions.assertThrows(LessThanOrEqualZeroAmountException.class, () -> checkingAccount.withdraw(accountId, givenAmount));

        verify(accountRepositoryMock).accountExists(accountId);
        verifyNoMoreInteractions(accountRepositoryMock);
    }

    @ParameterizedTest(name = "of {1} on account {0}")
    @MethodSource("lowerOrEqualThanZeroAmount")
    @DisplayName("Should not authorize deposit")
    void shouldNotAuthorizeLessThanOrEqualZeroDeposit(String accountId, BigDecimal givenAmount) {
        when(accountRepositoryMock.accountExists(accountId)).thenReturn(true);

        Assertions.assertThrows(LessThanOrEqualZeroAmountException.class, () -> checkingAccount.deposit(accountId, givenAmount));

        verify(accountRepositoryMock).accountExists(accountId);
        verifyNoMoreInteractions(accountRepositoryMock);
    }

    @Test
    @DisplayName("Should print history")
    void shouldPrintHistory() throws AccountNotFoundException {
        final var accountId = "837293";
        final List<Operation> existingOperation = Collections.emptyList();

        when(accountRepositoryMock.accountExists(accountId)).thenReturn(true);
        when(accountRepositoryMock.findOperations(accountId)).thenReturn(existingOperation);

        checkingAccount.printBankStatement(accountId, printerMock);

        verify(accountRepositoryMock).accountExists(accountId);
        verify(accountRepositoryMock).findOperations(accountId);
        verify(printerMock).print(existingOperation);
        verifyNoMoreInteractions(accountRepositoryMock, printerMock);
    }

}