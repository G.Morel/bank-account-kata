package kata;

import kata.exceptions.AccountNotFoundException;
import kata.exceptions.LessThanOrEqualZeroAmountException;

import java.math.BigDecimal;

public interface DepositsCapable {
    void deposit(String accountId, BigDecimal amount) throws LessThanOrEqualZeroAmountException, AccountNotFoundException;
}
