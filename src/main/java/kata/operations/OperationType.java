package kata.operations;

public enum OperationType {
    DEPOSIT,
    WITHDRAW
}
