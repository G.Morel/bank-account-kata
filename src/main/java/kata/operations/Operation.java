package kata.operations;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.Objects;

public class Operation {
    private final BigDecimal amount;
    private final BigDecimal balance;
    private final Instant date;
    private final OperationType type;

    public Operation(BigDecimal amount, BigDecimal balance, Instant date, OperationType type) {
        this.amount = amount.setScale(2, RoundingMode.HALF_UP);
        this.balance = balance.setScale(2, RoundingMode.HALF_UP);
        this.date = date;
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Instant getDate() {
        return date;
    }

    public OperationType getType() {
        return type;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Operation)) return false;
        Operation operation = (Operation) o;
        return Objects.equals(amount, operation.amount) &&
                Objects.equals(balance, operation.balance) &&
                Objects.equals(date, operation.date) &&
                type == operation.type;
    }

    @Override
    public final int hashCode() {
        return Objects.hash(amount, balance, date, type);
    }
}
