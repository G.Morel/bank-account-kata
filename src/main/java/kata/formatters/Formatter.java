package kata.formatters;

import kata.operations.Operation;

import java.util.List;

public interface Formatter {
    String formatOperations(List<Operation> operations);
}
