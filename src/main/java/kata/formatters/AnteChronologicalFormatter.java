package kata.formatters;

import kata.operations.Operation;
import kata.operations.OperationType;

import java.text.DecimalFormat;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AnteChronologicalFormatter implements Formatter {

    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy").withZone(ZoneOffset.UTC);
    private static final DecimalFormat decimalFormatter = new DecimalFormat("#0.00");
    private static final String OPERATION_TEMPLATE = "| %12s | %12s | %12s | %12s |";
    private static final String HEADER = String.format(OPERATION_TEMPLATE, "DATE", "CREDIT", "DEBIT", "BALANCE");

    private static String formatOperation(Operation operation) {
        final var formattedDate = dateFormatter.format(operation.getDate());
        final var formattedAmount = decimalFormatter.format(operation.getAmount());
        final var computedBalance = decimalFormatter.format(operation.getBalance());

        final var type = operation.getType();
        if (type.equals(OperationType.WITHDRAW)) {
            return String.format(OPERATION_TEMPLATE, formattedDate, "", formattedAmount, computedBalance);
        }
        if (type.equals(OperationType.DEPOSIT)) {
            return String.format(OPERATION_TEMPLATE, formattedDate, formattedAmount, "", computedBalance);
        }
        throw new UnsupportedOperationException("Not implemented for " + operation.getType() + " operation");
    }

    @Override
    public String formatOperations(List<Operation> operations) {
        if (operations == null) {
            return HEADER + System.lineSeparator();
        }
        final var lines = new ArrayList<>(Collections.singletonList(HEADER));
        final var sortedOperations = sortOperationsByDateDesc(operations);
        for (Operation operation : sortedOperations) {
            lines.add(formatOperation(operation));
        }
        return String.join(System.lineSeparator(), lines) + System.lineSeparator();
    }

    private static List<Operation> sortOperationsByDateDesc(List<Operation> operations) {
        return operations
                .stream()
                .sorted(Comparator.comparing(Operation::getDate).reversed())
                .collect(Collectors.toUnmodifiableList());
    }
}
