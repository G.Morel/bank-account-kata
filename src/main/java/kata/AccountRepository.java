package kata;

import kata.operations.Operation;

import java.math.BigDecimal;
import java.util.List;

public interface AccountRepository extends DepositsCapable, WithdrawalsCapable {
    boolean accountExists(String accountId);
    List<Operation> findOperations(String accountId);
    BigDecimal getBalance(String accountId);
}
