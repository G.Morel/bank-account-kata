package kata;

import kata.exceptions.AccountNotFoundException;
import kata.exceptions.LessThanOrEqualZeroAmountException;
import kata.exceptions.NotEnoughFundsException;
import kata.printers.Printer;

import java.math.BigDecimal;

public class DefaultCheckingAccount implements CheckingAccount {
    private final AccountRepository accountRepository;

    public DefaultCheckingAccount(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void deposit(String accountId, BigDecimal amount) throws AccountNotFoundException, LessThanOrEqualZeroAmountException {
        if (!accountRepository.accountExists(accountId)) {
            throw new AccountNotFoundException(accountId);
        }
        if (isLessThanOrEqualZero(amount)) {
            throw new LessThanOrEqualZeroAmountException(amount);
        }
        accountRepository.deposit(accountId, amount);
    }

    @Override
    public void withdraw(String accountId, BigDecimal amount) throws LessThanOrEqualZeroAmountException, AccountNotFoundException, NotEnoughFundsException {
        if (!accountRepository.accountExists(accountId)) {
            throw new AccountNotFoundException(accountId);
        }
        if (isLessThanOrEqualZero(amount)) {
            throw new LessThanOrEqualZeroAmountException(amount);
        }
        final var balance = accountRepository.getBalance(accountId);
        if (balance.compareTo(amount) < 0) {
            throw new NotEnoughFundsException();
        }
        accountRepository.withdraw(accountId, amount);
    }

    private boolean isLessThanOrEqualZero(BigDecimal amount) {
        return amount.compareTo(BigDecimal.ZERO) <= 0;
    }

    @Override
    public void printBankStatement(String accountId, Printer printer) throws AccountNotFoundException {
        if (!accountRepository.accountExists(accountId)) {
            throw new AccountNotFoundException(accountId);
        }
        final var operations = accountRepository.findOperations(accountId);
        printer.print(operations);
    }
}
