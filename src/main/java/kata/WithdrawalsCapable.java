package kata;

import kata.exceptions.AccountNotFoundException;
import kata.exceptions.LessThanOrEqualZeroAmountException;
import kata.exceptions.NotEnoughFundsException;

import java.math.BigDecimal;

public interface WithdrawalsCapable {
    void withdraw(String accountId, BigDecimal amount) throws LessThanOrEqualZeroAmountException, NotEnoughFundsException, AccountNotFoundException;
}
