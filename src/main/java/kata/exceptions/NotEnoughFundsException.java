package kata.exceptions;

public class NotEnoughFundsException extends Exception {
    public NotEnoughFundsException() {
        super("Account not enough funded");
    }
}
