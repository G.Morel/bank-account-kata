package kata.exceptions;

public class AccountNotFoundException extends Exception {


    public AccountNotFoundException(String accountId) {
        super(String.format("Account %s does not exist", accountId));
    }
}
