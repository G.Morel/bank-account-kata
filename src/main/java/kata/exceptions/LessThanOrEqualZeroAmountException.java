package kata.exceptions;

import java.math.BigDecimal;

public class LessThanOrEqualZeroAmountException extends Exception {
    public LessThanOrEqualZeroAmountException(BigDecimal amount) {
        super(String.format("%f is not greater than zero", amount));
    }
}
