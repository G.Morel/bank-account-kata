package kata;

import kata.exceptions.AccountNotFoundException;
import kata.printers.Printer;

public interface CheckingAccount extends WithdrawalsCapable, DepositsCapable {
    void printBankStatement(String accountId, Printer printer) throws AccountNotFoundException;
}
