package kata.printers;

import kata.operations.Operation;

import java.util.List;

public interface Printer {
    void print(List<Operation> operations);
}
