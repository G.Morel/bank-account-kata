package kata.printers;

import kata.formatters.Formatter;
import kata.operations.Operation;

import java.io.PrintStream;
import java.util.List;

public class StreamPrinter implements Printer {

    private final PrintStream printStream;
    private final Formatter formatter;

    public StreamPrinter(PrintStream printStream, Formatter formatter) {
        this.printStream = printStream;
        this.formatter = formatter;
    }

    @Override
    public void print(List<Operation> operations) {
        final var formattedStatement = formatter.formatOperations(operations);
        printStream.print(formattedStatement);
    }
}
